SRC := src
BUILD := build

LOXDIR := dev/jgoble/lox
TOOLDIR := dev/jgoble/tool

TOOLSOURCES := $(shell find $(SRC)/$(TOOLDIR) -type f -name "*.java")
TOOLCLASSES := $(patsubst $(SRC)/%.java, $(BUILD)/%.class, $(TOOLSOURCES))

GENERATED := Expr.java Stmt.java
GENERATED_SOURCES := $(addprefix $(SRC)/$(LOXDIR)/ast/, $(GENERATED))
SOURCES := $(sort $(shell find $(SRC)/$(LOXDIR) -type f -name "*.java") $(GENERATED_SOURCES))
CLASSES := $(patsubst $(SRC)/%.java, $(BUILD)/%.class, $(SOURCES))

EXE := jlox
JARFILE := lox.jar

.SUFFIXES: .java .class

.PHONY: all clean test regen

all: $(EXE)

# The executable
$(EXE): $(BUILD)/$(JARFILE)
	echo \#\!/usr/bin/java -jar > $@
	cat $< >> $@
	chmod +x $@

# .jar archive
$(BUILD)/$(JARFILE): $(CLASSES)
	set -e; cd $(BUILD); \
	ALLCLASSES=$$(find $(LOXDIR) -type f -name '*.class'); \
	jar -cfe $(JARFILE) dev.jgoble.lox.Lox $$ALLCLASSES

# Individual class files
$(CLASSES) $(TOOLCLASSES): $(BUILD)/%.class: $(SRC)/%.java
	javac -d $(BUILD) -sourcepath $(SRC) $<

# Regenerate generated files
$(GENERATED_SOURCES) regen: $(BUILD)/$(TOOLDIR)/GenerateAst.class
	cd build && java $(TOOLDIR)/GenerateAst ../$(SRC)/$(LOXDIR)/ast

clean: justclean regen

justclean:
	rm -rf $(BUILD) $(EXE) $(GENERATED_SOURCES)

test: $(EXE)
	./$(EXE) test.lox
