package dev.jgoble.lox.exceptions;

import dev.jgoble.lox.internal.Token;

@SuppressWarnings("serial")
public class RuntimeError extends RuntimeException {
    public final Token token;

    public RuntimeError(Token token, String message) {
        super(message);
        this.token = token;
    }
}
