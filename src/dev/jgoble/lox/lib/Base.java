package dev.jgoble.lox.lib;

import java.util.Arrays;
import java.util.List;

import dev.jgoble.lox.internal.Environment;
import dev.jgoble.lox.types.JavaFunction;
import dev.jgoble.lox.types.LoxCallable;
import dev.jgoble.lox.types.LoxClass;
import dev.jgoble.lox.types.LoxFloat;
import dev.jgoble.lox.types.LoxInstance;
import dev.jgoble.lox.types.LoxString;
import dev.jgoble.lox.types.LoxStringClass;

public class Base {
    private static List<LoxCallable> functions = Arrays.asList(
        new JavaFunction("clock", 0, (interp, args, env) -> {
            return new LoxFloat((double)System.currentTimeMillis() / 1000.0);
        }),
        new JavaFunction("print", 1, (interp, args, env) -> {
            System.out.println(interp.stringify(args.get(0)));
            return null;
        }),
        new JavaFunction("type", 1, (interp, args, env) -> {
            Object object = args.get(0);
            if (object == null) {
                return new LoxString("nil");
            } else if (object instanceof LoxFloat) {
                return new LoxString("float");
            } else if (object instanceof LoxString) {
                return new LoxString("string");
            } else if (object instanceof LoxClass) {
                return new LoxString("class");
            } else if (object instanceof LoxCallable) {
                return new LoxString("function");
            } else if (object instanceof Boolean) {
                return new LoxString("boolean");
            } else if (object instanceof LoxInstance) {
                return new LoxString("instance");
            } else {
                throw new RuntimeException("Unreachable code");
            }
        }),
        LoxStringClass.getInstance()
    );

    public static void loadLibrary(Environment environment) {
        for (LoxCallable function : functions) {
            environment.define(function.getName(), function);
        }
    }
}
