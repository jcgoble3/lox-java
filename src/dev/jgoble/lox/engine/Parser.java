package dev.jgoble.lox.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dev.jgoble.lox.Lox;
import dev.jgoble.lox.ast.Expr;
import dev.jgoble.lox.ast.Stmt;
import dev.jgoble.lox.internal.Token;
import dev.jgoble.lox.internal.TokenType;
import dev.jgoble.lox.types.LoxFloat;
import dev.jgoble.lox.types.LoxString;

import static dev.jgoble.lox.internal.TokenType.*;

public class Parser {
    @SuppressWarnings("serial")
    private static class ParseError extends RuntimeException {}

    private final List<Token> tokens;
    private int current = 0;

    public Parser(List<Token> tokens) {
        this.tokens = tokens;
    }

    public List<Stmt> parse() {
        List<Stmt> statements = new ArrayList<>();
        while (!isAtEnd()) {
            statements.add(declaration());
        }
        return statements;
    }

    private Stmt declaration() {
        try {
            if (match(TOKEN_CLASS)) {
                return classDeclaration();
            }
            if (match(TOKEN_FUN)) {
                return function("function");
            }
            if (match(TOKEN_VAR)) {
                return varDeclaration();
            }
            return statement();
        } catch (ParseError error) {
            synchronize();
            return null;
        }
    }

    private Stmt classDeclaration() {
        Token name = consume(TOKEN_IDENTIFIER, "Expected class name");
        Expr.Variable superclass = null;
        if (match(TOKEN_LESS)) {
            consume(TOKEN_IDENTIFIER, "Expected superclass name after '<'");
            superclass = new Expr.Variable(previous());
        }
        consume(TOKEN_LEFT_BRACE, "Expected '{' after class name");
        List<Stmt.Function> methods = new ArrayList<>();
        while (!check(TOKEN_RIGHT_BRACE) && !isAtEnd()) {
            methods.add(function("method"));
        }
        consume(TOKEN_RIGHT_BRACE, "Expected '}' after class body");
        return new Stmt.Class(name, superclass, methods);
    }

    private Stmt.Function function(String kind) {
        Token name = consume(TOKEN_IDENTIFIER, "Expected " + kind + " name");
        consume(TOKEN_LEFT_PAREN, "Expected '(' after " + kind + " name");
        List<Token> parameters = new ArrayList<>();
        if (!check(TOKEN_RIGHT_PAREN)) {
            do {
                if (parameters.size() >= 255) {
                    error(peek(), "Parameter list exceeds limit of 255 parameters");
                }
                parameters.add(consume(TOKEN_IDENTIFIER, "Expected parameter name"));
            } while (match(TOKEN_COMMA));
        }
        consume(TOKEN_RIGHT_PAREN, "Expected ')' after parameters");
        consume(TOKEN_LEFT_BRACE, "Expected '{' before " + kind + " body");
        List<Stmt> body = block();
        return new Stmt.Function(name, parameters, body);
    }

    private Stmt varDeclaration() {
        Token name = consume(TOKEN_IDENTIFIER, "Expected variable name");
        Expr initializer = match(TOKEN_EQUAL) ? expression() : null;
        consume(TOKEN_SEMICOLON, "Expected ';' after declaration");
        return new Stmt.Var(name, initializer);
    }

    private Stmt statement() {
        //if (match(TOKEN_PRINT)) {
        //    return printStatement();
        //}
        if (match(TOKEN_RETURN)) {
            return returnStatement();
        }
        if (match(TOKEN_LEFT_BRACE)) {
            return new Stmt.Block(block());
        }
        if (match(TOKEN_IF)) {
            return ifStatement();
        }
        if (match(TOKEN_WHILE)) {
            return whileStatement();
        }
        if (match(TOKEN_FOR)) {
            return forStatement();
        }
        if (match(TOKEN_BREAK)) {
            return breakStatement();
        }
        if (match(TOKEN_CONTINUE)) {
            return continueStatement();
        }
        return expressionStatement();
    }

    private List<Stmt> block() {
        List<Stmt> statements = new ArrayList<>();
        while (!check(TOKEN_RIGHT_BRACE) && !isAtEnd()) {
            statements.add(declaration());
        }
        consume(TOKEN_RIGHT_BRACE, "Expected '}' to close block");
        return statements;
    }

    private Stmt returnStatement() {
        Token keyword = previous();
        Expr value = !check(TOKEN_SEMICOLON) ? value = expression() : null;
        consume(TOKEN_SEMICOLON, "Expected ';' after return statement");
        return new Stmt.Return(keyword, value);
    }

    private Stmt ifStatement() {
        Expr condition = expression();
        consume(TOKEN_LEFT_BRACE, "Expected block after condition");
        Stmt thenBranch = new Stmt.Block(block());
        Stmt elseBranch = null;
        if (match(TOKEN_ELSE)) {
            consume(TOKEN_LEFT_BRACE, "Expected block after 'else'");
            elseBranch = new Stmt.Block(block());
        }
        return new Stmt.If(condition, thenBranch, elseBranch);
    }

    private Stmt whileStatement() {
        consume(TOKEN_LEFT_PAREN, "Expected '(' after 'while'");
        Expr condition = expression();
        consume(TOKEN_RIGHT_PAREN, "Expected ')' after condition");
        Stmt body = statement();
        return new Stmt.While(condition, body);
    }

    private Stmt breakStatement() {
        Token keyword = previous();
        consume(TOKEN_SEMICOLON, "Expected ';' after 'break'");
        return new Stmt.Break(keyword);
    }

    private Stmt continueStatement() {
        Token keyword = previous();
        consume(TOKEN_SEMICOLON, "Expected ';' after 'continue'");
        return new Stmt.Continue(keyword);
    }

    private Stmt forStatement() {
        consume(TOKEN_LEFT_PAREN, "Expected '(' after 'for'");
        Stmt initializer;
        if (match(TOKEN_SEMICOLON)) {
            initializer = null;
        } else if (match(TOKEN_VAR)) {
            initializer = varDeclaration();
        } else {
            initializer = expressionStatement();
        }
        Expr condition = !check(TOKEN_SEMICOLON) ? expression() : new Expr.Literal(true);
        consume(TOKEN_SEMICOLON, "Expected ';' after loop condition");
        Expr increment = !check(TOKEN_RIGHT_PAREN) ? expression() : null;
        consume(TOKEN_RIGHT_PAREN, "Expected ')' after loop clauses");
        Stmt body = statement();

        if (increment != null) {
            body = new Stmt.Block(Arrays.asList(body, new Stmt.Expression(increment)));
        }
        body = new Stmt.While(condition, body);
        if (initializer != null) {
            body = new Stmt.Block(Arrays.asList(initializer, body));
        }

        return body;
    }

    private Stmt expressionStatement() {
        Expr expr = expression();
        consume(TOKEN_SEMICOLON, "Expected ';' after expression");
        return new Stmt.Expression(expr);
    }

    private Expr expression() {
        return assignment();
    }

    private Expr assignment() {
        Expr expr = or();
        if (match(TOKEN_EQUAL)) {
            Token equals = previous();
            Expr value = assignment();
            if (expr instanceof Expr.Variable) {
                Token name = ((Expr.Variable)expr).name;
                return new Expr.Assign(name, value);
            } else if (expr instanceof Expr.Get) {
                Expr.Get get = (Expr.Get)expr;
                return new Expr.Set(get.object, get.name, value);
            }
            error(equals, "Invalid assignment target");
        }
        return expr;
    }

    private Expr or() {
        Expr expr = and();
        while (match(TOKEN_OR)) {
            Token operator = previous();
            Expr right = and();
            expr = new Expr.Logical(expr, operator, right);
        }
        return expr;
    }

    private Expr and() {
        Expr expr = equality();
        while (match(TOKEN_AND)) {
            Token operator = previous();
            Expr right = equality();
            expr = new Expr.Logical(expr, operator, right);
        }
        return expr;
    }

    private Expr equality() {
        Expr expr = comparison();
        while (match(TOKEN_BANG_EQUAL, TOKEN_EQUAL_EQUAL)) {
            Token operator = previous();
            Expr right = comparison();
            expr = new Expr.Binary(expr, operator, right);
        }
        return expr;
    }

    private Expr comparison() {
        Expr expr = term();
        while (match(TOKEN_GREATER, TOKEN_GREATER_EQUAL, TOKEN_LESS, TOKEN_LESS_EQUAL)) {
            Token operator = previous();
            Expr right = term();
            expr = new Expr.Binary(expr, operator, right);
        }
        return expr;
    }

    private Expr term() {
        Expr expr = factor();
        while (match(TOKEN_MINUS, TOKEN_PLUS)) {
            Token operator = previous();
            Expr right = factor();
            expr = new Expr.Binary(expr, operator, right);
        }
        return expr;
    }

    private Expr factor() {
        Expr expr = unary();
        while (match(TOKEN_SLASH, TOKEN_STAR, TOKEN_PERCENT)) {
            Token operator = previous();
            Expr right = unary();
            expr = new Expr.Binary(expr, operator, right);
        }
        return expr;
    }

    private Expr unary() {
        if (match(TOKEN_BANG, TOKEN_MINUS)) {
            Token operator = previous();
            Expr right = unary();
            return new Expr.Unary(operator, right);
        }
        return exponent();
    }

    private Expr exponent() {
        Expr expr = call();
        if (match(TOKEN_CARET)) {
            Token operator = previous();
            Expr right = exponent();
            return new Expr.Binary(expr, operator, right);
        }
        return expr;
    }

    private Expr call() {
        Expr expr = primary();
        while (true) {
            if (match(TOKEN_LEFT_PAREN)) {
                expr = finishCall(expr);
            } else if (match(TOKEN_DOT)) {
                Token name = consume(TOKEN_IDENTIFIER, "Expected property name after '.'");
                expr = new Expr.Get(expr, name);
            } else {
                break;
            }
        }
        return expr;
    }

    private Expr finishCall(Expr callee) {
        List<Expr> arguments = new ArrayList<>();
        if (!check(TOKEN_RIGHT_PAREN)) {
            do {
                if (arguments.size() >= 255) {
                    error(peek(), "Call exceeds limit of 255 arguments");
                }
                arguments.add(expression());
            } while (match(TOKEN_COMMA));
        }
        Token paren = consume(TOKEN_RIGHT_PAREN, "Expected ')' after arguments");
        return new Expr.Call(callee, paren, arguments);
    }

    private Expr primary() {
        if (match(TOKEN_FALSE)) {
            return new Expr.Literal(false);
        }
        if (match(TOKEN_TRUE)) {
            return new Expr.Literal(true);
        }
        if (match(TOKEN_NIL)) {
            return new Expr.Literal(null);
        }
        if (match(TOKEN_NUMBER)) {
            return new Expr.Literal(new LoxFloat((double)previous().literal));
        }
        if (match(TOKEN_STRING)) {
            return new Expr.Literal(new LoxString((String)previous().literal));
        }
        if (match(TOKEN_LEFT_PAREN)) {
            Expr expr = expression();
            consume(TOKEN_RIGHT_PAREN, "Expected ')' after expression");
            return new Expr.Grouping(expr);
        }
        if (match(TOKEN_THIS)) {
            return new Expr.This(previous());
        }
        if (match(TOKEN_SUPER)) {
            Token keyword = previous();
            consume(TOKEN_DOT, "Expected '.' after 'super'");
            Token method = consume(TOKEN_IDENTIFIER, "Expected method name");
            return new Expr.Super(keyword, method);
        }
        if (match(TOKEN_IDENTIFIER)) {
            return new Expr.Variable(previous());
        }
        throw error(peek(), "Expected expression");
    }

    private boolean match(TokenType... types) {
        for (TokenType type : types) {
            if (check(type)) {
                advance();
                return true;
            }
        }
        return false;
    }

    private Token consume(TokenType type, String message) {
        if (check(type)) {
            return advance();
        }
        throw error(peek(), message);
    }

    private boolean check(TokenType type) {
        if (isAtEnd()) {
            return false;
        }
        return peek().type == type;
    }

    private Token advance() {
        if (!isAtEnd()) {
            current++;
        }
        return previous();
    }

    private boolean isAtEnd() {
        return peek().type == TOKEN_EOF;
    }

    private Token peek() {
        return tokens.get(current);
    }

    private Token previous() {
        return tokens.get(current - 1);
    }

    private ParseError error(Token token, String message) {
        Lox.error(token, message);
        return new ParseError();
    }

    private void synchronize() {
        advance();
        while (!isAtEnd()) {
            if (previous().type == TOKEN_SEMICOLON) {
                return;
            }
            switch (peek().type) {
                case TOKEN_CLASS:
                case TOKEN_FUN:
                case TOKEN_VAR:
                case TOKEN_FOR:
                case TOKEN_IF:
                case TOKEN_WHILE:
                case TOKEN_RETURN:
                    return;
                default:
                    advance();
            }
        }
    }
}
