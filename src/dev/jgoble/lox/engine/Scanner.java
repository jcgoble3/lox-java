package dev.jgoble.lox.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.jgoble.lox.Lox;
import dev.jgoble.lox.internal.Token;
import dev.jgoble.lox.internal.TokenType;

import static dev.jgoble.lox.internal.TokenType.*;

public class Scanner {
    private final String source;
    private final List<Token> tokens = new ArrayList<>();
    private int start = 0;
    private int current = 0;
    private int line = 1;
    private static final Map<String, TokenType> keywords;

    static {
        keywords = new HashMap<>();
        keywords.put("and", TOKEN_AND);
        keywords.put("break", TOKEN_BREAK);
        keywords.put("class", TOKEN_CLASS);
        keywords.put("continue", TOKEN_CONTINUE);
        keywords.put("else", TOKEN_ELSE);
        keywords.put("false", TOKEN_FALSE);
        keywords.put("for", TOKEN_FOR);
        keywords.put("fun", TOKEN_FUN);
        keywords.put("if", TOKEN_IF);
        keywords.put("nil", TOKEN_NIL);
        keywords.put("or", TOKEN_OR);
        keywords.put("return", TOKEN_RETURN);
        keywords.put("super", TOKEN_SUPER);
        keywords.put("this", TOKEN_THIS);
        keywords.put("true", TOKEN_TRUE);
        keywords.put("var", TOKEN_VAR);
        keywords.put("while", TOKEN_WHILE);
    }

    public Scanner(String source) {
        this.source = source;
    }

    public List<Token> scanTokens() {
        while (!isAtEnd()) {
            // We are at the beginning of the next lexeme
            start = current;
            scanToken();
        }

        tokens.add(new Token(TOKEN_EOF, "", null, line));
        return tokens;
    }

    private void scanToken() {
        char c = advance();
        switch (c) {
            case '(': addToken(TOKEN_LEFT_PAREN); break;
            case ')': addToken(TOKEN_RIGHT_PAREN); break;
            case '{': addToken(TOKEN_LEFT_BRACE); break;
            case '}': addToken(TOKEN_RIGHT_BRACE); break;
            case ',': addToken(TOKEN_COMMA); break;
            case '.': addToken(TOKEN_DOT); break;
            case '-': addToken(TOKEN_MINUS); break;
            case '+': addToken(TOKEN_PLUS); break;
            case ';': addToken(TOKEN_SEMICOLON); break;
            case '*': addToken(TOKEN_STAR); break;
            case '^': addToken(TOKEN_CARET); break;
            case '%': addToken(TOKEN_PERCENT); break;
            case '!':
                addToken(match('=') ? TOKEN_BANG_EQUAL : TOKEN_BANG);
                break;
            case '=':
                addToken(match('=') ? TOKEN_EQUAL_EQUAL : TOKEN_EQUAL);
                break;
            case '<':
                addToken(match('=') ? TOKEN_LESS_EQUAL : TOKEN_LESS);
                break;
            case '>':
                addToken(match('=') ? TOKEN_GREATER_EQUAL : TOKEN_GREATER);
                break;
            case '/':
                if (match('/')) {
                    // A comment goes until the end of the line
                    while (peek() != '\n' && !isAtEnd()) {
                        advance();
                    }
                } else {
                    addToken(TOKEN_SLASH);
                }
                break;
            case '"': string(); break;
            case '0':
                if (match('x') || match('X')) {
                    hexNumber();
                    break;
                } // else FALLTHROUGH
            case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9':
                number();
                break;

            // Ignore whitespace
            case ' ': case '\r': case '\t': break;
            case '\n': line++; break;

            default:
                if (isAlpha(c)) {
                    identifier();
                } else {
                    Lox.error(line, "Unexpected character: " + c);
                }
                break;
        }
    }

    private void identifier() {
        while (isAlphaNumeric(peek())) {
            advance();
        }
        String text = source.substring(start, current);
        TokenType type = keywords.get(text);
        if (type == null) {
            type = TOKEN_IDENTIFIER;
        }
        addToken(type);
    }

    private void number() {
        while (isDigit(peek())) {
            advance();
        }
        // Look for a fractional part
        if (peek() == '.' && isDigit(peekNext())) {
            advance();
            while (isDigit(peek())) {
                advance();
            }
        }
        double value = Double.parseDouble(source.substring(start, current));
        if (peek() == 'e' || peek() == 'E') {
            advance();
            int sign = 1;
            if (match('-')) {
                sign = -1;
            } else {
                match('+');
            }
            if (!isDigit(peek())) {
                Lox.error(line, "Invalid number literal");
                return;
            }
            int mark = current;
            while (isDigit(peek())) {
                advance();
            }
            int exponent = sign * Integer.parseInt(source.substring(mark, current));
            value = value * Math.pow(10, exponent);
        }

        addToken(TOKEN_NUMBER, value);
    }

    private void hexNumber() {
        double value = 0.0;
        int digit;
        while ((digit = hexDigit(peek())) >= 0) {
            value = value * 16 + digit;
            advance();
        }
        addToken(TOKEN_NUMBER, value);
    }

    private void string() {
        StringBuilder builder = new StringBuilder();
        int mark = start + 1;
        while (peek() != '"' && !isAtEnd()) {
            switch (peek()) {
                case '\n':
                    Lox.error(line, "EOL while scanning string");
                    return;
                case '\\':
                    // Escape sequence
                    builder.append(source.substring(mark, current));
                    advance();
                    switch (advance()) {
                        case '\\': builder.append('\\'); break;
                        case '"': builder.append('"'); break;
                        case '\n': line++; // FALLTHROUGH
                        case 'n': builder.append('\n'); break;
                        case 'r': builder.append('\r'); break;
                        case 't': builder.append('\t'); break;
                        case '0': builder.append('\0'); break;
                        case 'x': {
                            int d1 = hexDigit(advance());
                            int d2 = hexDigit(advance());
                            if (d1 < 0 || d2 < 0) {
                                Lox.error(line, "Invalid hex escape");
                                return;
                            }
                            builder.append((char)(d1 * 16 + d2));
                            break;
                        }
                        case '\0':
                            Lox.error(line, "Invalid backslash");
                            return;
                        default:
                            Lox.error(line, "Invalid escape sequence");
                            return;
                    }
                    mark = current;
                    break;
                default:
                    advance();
            }
        }
        if (isAtEnd()) {
            Lox.error(line, "Unterminated string");
            return;
        }
        // Append the rest of the string
        builder.append(source.substring(mark, current));
        // Consume the closing "
        advance();
        addToken(TOKEN_STRING, builder.toString());
    }

    private int hexDigit(char digit) {
        if (isDigit(digit)) {
            return digit - '0';
        } else if (digit >= 'a' && digit <= 'f') {
            return digit - 'a' + 10;
        } else if (digit >= 'A' && digit <= 'F') {
            return digit - 'A' + 10;
        } else {
            return -1; // Error
        }
    }

    private boolean match(char expected) {
        if (isAtEnd()) {
            return false;
        }
        if (source.charAt(current) != expected) {
            return false;
        }
        current++;
        return true;
    }

    private char peek() {
        if (isAtEnd()) {
            return '\0';
        }
        return source.charAt(current);
    }

    private char peekNext() {
        if (current + 1 >= source.length()) {
            return '\0';
        }
        return source.charAt(current + 1);
    }

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private boolean isAlpha(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
    }

    private boolean isAlphaNumeric(char c) {
        return isAlpha(c) || isDigit(c);
    }

    private boolean isAtEnd() {
        return current >= source.length();
    }

    private char advance() {
        if (isAtEnd()) {
            return '\0';
        }
        current++;
        return source.charAt(current - 1);
    }

    private void addToken(TokenType type) {
        addToken(type, null);
    }

    private void addToken(TokenType type, Object literal) {
        String text = source.substring(start, current);
        tokens.add(new Token(type, text, literal, line));
    }
}
