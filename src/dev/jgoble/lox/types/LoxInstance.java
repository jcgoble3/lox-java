package dev.jgoble.lox.types;

import java.util.HashMap;
import java.util.Map;

import dev.jgoble.lox.exceptions.RuntimeError;
import dev.jgoble.lox.internal.Token;

public class LoxInstance {
    protected LoxClass klass;
    private final Map<String, Object> fields = new HashMap<>();

    LoxInstance(LoxClass klass) {
        this.klass = klass;
    }

    public Object get(Token name) {
        if (fields.containsKey(name.lexeme)) {
            return fields.get(name.lexeme);
        }
        Function method = klass.findMethod(name.lexeme);
        if (method != null) {
            return method.bind(this);
        }
        throw new RuntimeError(name, "Undefined property '" + name.lexeme + "'");
    }

    public void set(Token name, Object value) {
        fields.put(name.lexeme, value);
    }

    void set(String name, Object value) {
        fields.put(name, value);
    }

    @Override
    public String toString() {
        return klass.name + " instance";
    }
}
