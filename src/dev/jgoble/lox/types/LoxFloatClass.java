package dev.jgoble.lox.types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.jgoble.lox.engine.Interpreter;

public class LoxFloatClass extends LoxClass {
    private static LoxFloatClass singleton = null;

    private LoxFloatClass(String name, LoxClass superclass, Map<String, Function> methods) {
        super(name, superclass, methods);
    }

    public static LoxFloatClass getInstance() {
        if (singleton == null) {
            Map<String, Function> methods = new HashMap<>();
            singleton = new LoxFloatClass("Float", null, methods);
        }
        return singleton;
    }

    @Override
    public int arity() {
        return 1;
    }

    @Override
    public Object call(Interpreter interpreter, List<Object> arguments) {
        Object arg = arguments.get(0);
        if (arg instanceof Double) {
            return new LoxFloat((double)arg);
        }
        if (arg instanceof LoxFloat) {
            return arg;
        }
        return new LoxFloat(0.0);
    }
}
