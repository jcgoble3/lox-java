package dev.jgoble.lox.types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.jgoble.lox.engine.Interpreter;

public class LoxStringClass extends LoxClass {
    private static LoxStringClass singleton = null;

    private LoxStringClass(String name, LoxClass superclass, Map<String, Function> methods) {
        super(name, superclass, methods);
    }

    public static LoxStringClass getInstance() {
        if (singleton == null) {
            Map<String, Function> methods = new HashMap<>();
            singleton = new LoxStringClass("String", null, methods);
        }
        return singleton;
    }

    @Override
    public int arity() {
        return 1;
    }

    @Override
    public Object call(Interpreter interpreter, List<Object> arguments) {
        return new LoxString(interpreter.stringify(arguments.get(0)));
    }
}
