package dev.jgoble.lox.types;

public class LoxFloat extends LoxInstance {
    public final double value;

    public LoxFloat(double value) {
        super(LoxFloatClass.getInstance());
        this.value = value;
    }

    @Override
    public String toString() {
        String text = Double.valueOf(value).toString();
        if (text.endsWith(".0")) {
            text = text.substring(0, text.length() - 2);
        }
        return text;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LoxFloat) {
            return value == ((LoxFloat)obj).value;
        }
        return super.equals(obj);
    }

    public LoxFloat __negate() {
        return new LoxFloat(-value);
    }

    public LoxFloat __subtract(LoxFloat right) {
        return new LoxFloat(value - right.value);
    }

    public LoxFloat __divide(LoxFloat right) {
        return new LoxFloat(value / right.value);
    }

    public LoxFloat __multiply(LoxFloat right) {
        return new LoxFloat(value * right.value);
    }

    public LoxFloat __mod(LoxFloat right) {
        return new LoxFloat((double)Math.floorMod((long)value, (long)right.value));
    }

    public LoxFloat __pow(LoxFloat right) {
        return new LoxFloat(Math.pow(value, right.value));
    }

    public LoxFloat __add(LoxFloat right) {
        return new LoxFloat(value + right.value);
    }

    public boolean __gt(LoxFloat right) {
        return value > right.value;
    }

    public boolean __ge(LoxFloat right) {
        return value >= right.value;
    }

    public boolean __lt(LoxFloat right) {
        return value < right.value;
    }

    public boolean __le(LoxFloat right) {
        return value <= right.value;
    }
}
