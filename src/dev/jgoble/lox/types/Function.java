package dev.jgoble.lox.types;

public interface Function extends LoxCallable {
    Function bind(LoxInstance instance);
}
